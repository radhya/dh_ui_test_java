# DH_UI_Test_JAVA

This is a test project for UI testing for Docler Holding in Selenium Webdriver with Java Language.

Prerequiste:
Chrome Browser: https://www.google.com/intl/en_in/chrome/

Chrome Driver: https://chromedriver.chromium.org/

Maven: https://maven.apache.org/download.cgi

Java: https://www.java.com/en/download/

Allure(Report): https://docs.qameta.io/allure/

Please set environment variable and path for Maven and Java.

Step 1: Clone the code from git repository

Step 2: You will have file "chromedriver" you need to download chromedriver from https://chromedriver.chromium.org/ acording your OS and browser version and replace it.

Step 3: Now go to command prompt and set project repository as based.

Step 4: Run command 'mvn clean test'. This will start executing test in chrome browser.

Step 5: After successfull execute, run command 'allure serve allure-results' to generate Allure Report.

Note: I have set video 'uitestjava.mp4' for execution process and generate report. Please watch it if you not clear with above steps.