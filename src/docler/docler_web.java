package docler;

import org.testng.annotations.Test;
import master.field_master;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import java.net.HttpURLConnection;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class docler_web extends field_master {
	
protected String uititle;
	
@Story("Title - UI Testing Site")
@Description("Title - UI Testing Site")
@Severity(SeverityLevel.MINOR)
@Test (priority = 1, description = "Title - UI Testing Site", enabled=true)
public void require_ui_1_title_verify() throws Exception, Throwable {
	
			uititle = driver.getTitle();
			
			if(uititle.contentEquals("UI Testing Site"))
			{	
				TestReason = "Pass: Home Page Title verify successfully";
				logger.info(TestReason); 
				myscreenshot(driver);
			}			
			else
			{	
				TestReason = "Fail: Home Page Title display incorrect";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}
			
			form_menu_link();
			Thread.sleep(1000);
			if(uititle.contentEquals("UI Testing Site"))
			{	
				TestReason = "Pass: Form Page Title verify successfully";
				logger.info(TestReason); 
				myscreenshot(driver);
			}			
			else
			{	
				TestReason = "Fail: Form Page Title display incorrect";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}
			
}
@Story("Company Logo Verification")
@Description("Company Logo Verification")
@Severity(SeverityLevel.MINOR)
@Test (priority = 2, description = "Company Logo Verification", enabled=true)
public void require_ui_2_logo_verify() throws Exception, Throwable {
			home_menu_link();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("logo_link"))));
				if(driver.findElements(By.xpath(obj.getProperty("logo_link"))).size() != 0)
				{
					TestReason = "Pass: Logo present on Home page.";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Logo not present on Home page";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Logo not present on Home page";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
			form_menu_link();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("logo_link"))));
				if(driver.findElements(By.xpath(obj.getProperty("logo_link"))).size() != 0)
				{
					TestReason = "Pass: Logo present on Form page.";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Logo not present on Form page";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Logo not present on Form page";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Home Page Navigation Verification")
@Description("Home Page Navigation Verification")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 3, description = "Home Page Navigation Verification", enabled=true)
public void require_ui_3_home_navigate_verify() throws Exception, Throwable {
			home_menu_link();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("home_welcome_contain"))));
				if(driver.findElements(By.xpath(obj.getProperty("home_welcome_contain"))).size() != 0)
				{
					TestReason = "Pass: Home page navigate properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Home page not navigate properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Home page not navigate properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Home Active Status Verification")
@Description("Home Active Status Verification")
@Severity(SeverityLevel.TRIVIAL)
@Test (priority = 4, description = "Home Active Status Verification", enabled=true)
public void require_ui_4_home_active_status_verify() throws Exception, Throwable {
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("home_active_menu"))));
				if(driver.findElements(By.xpath(obj.getProperty("home_active_menu"))).size() != 0)
				{
					TestReason = "Pass: Home menu Active status display properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Home menu Active status not display";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Home menu Active status not display";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Form Page Navigation Verification")
@Description("Form Page Navigation Verification")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 5, description = "Form Page Navigation Verification", enabled=true)
public void require_ui_5_form_navigate_verify() throws Exception, Throwable {
			form_menu_link();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("form_contain"))));
				if(driver.findElements(By.xpath(obj.getProperty("form_contain"))).size() != 0)
				{
					TestReason = "Pass: Form page navigate properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Form page not navigate properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Form page not navigate properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Form Active Status Verification")
@Description("Form Active Status Verification")
@Severity(SeverityLevel.TRIVIAL)
@Test (priority = 6, description = "Form Active Status Verification", enabled=true)
public void require_ui_6_form_active_status_verify() throws Exception, Throwable {
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("form_active_menu"))));
				if(driver.findElements(By.xpath(obj.getProperty("form_active_menu"))).size() != 0)
				{
					TestReason = "Pass: Form menu Active status display properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Form menu Active status not display";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Form menu Active status not display";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Error 404 Status Verification")
@Description("Error 404 Status Verification")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 7, description = "Error 404 Status Verification", enabled=true)
public void require_ui_7_error_404_status_verify() throws Exception, Throwable {
			error_menu_link();
			String linkUrl = driver.getCurrentUrl();
			URL url = new URL(linkUrl);
			logger.info("Error Menu URL : " + linkUrl);

			HttpURLConnection httpURLConnect = (HttpURLConnection) url.openConnection();
			httpURLConnect.setConnectTimeout(25000);
			httpURLConnect.connect();

			if (httpURLConnect.getResponseCode() == 404)
			{
				TestReason = linkUrl + " - " + httpURLConnect.getResponseMessage();
				logger.info(TestReason);
				TestReason = "Pass: Error code 404 received properly";
				logger.info(TestReason);
				myscreenshot(driver);							
			}
			else
			{
				TestReason = "Fail : User not received 404 error response";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}
}
@Story("Logo Redirect Home Verification")
@Description("Logo Redirect Home Verification")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 8, description = "Logo Redirecct Home Verification", enabled=true)
public void require_ui_8_logo_redirect_home_verify() throws Exception, Throwable {
			driver.navigate().back();
			dockler_logo();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("home_welcome_contain"))));
				if(driver.findElements(By.xpath(obj.getProperty("home_welcome_contain"))).size() != 0)
				{
					TestReason = "Pass: Home page navigate properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Home page not navigate properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Home page not navigate properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Home Page H1 Text Visible Verification")
@Description("Home Page H1 Text Visible Verification")
@Severity(SeverityLevel.TRIVIAL)
@Test (priority = 9, description = "Home Page H1 Text Visible Verification", enabled=true)
public void require_ui_9_home_h1_text_verify() throws Exception, Throwable {
			home_menu_link();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("home_welcome_contain"))));
				String h1text = driver.findElement(By.xpath(obj.getProperty("home_welcome_contain"))).getText();
				if(h1text.contentEquals("Welcome to the Docler Holding QA Department"))
				{
					TestReason = "Pass: Home page H1 text visible properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Home page H1 text not visible properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Home page H1 text not visible properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Home Page P Tag Text Visible Verification")
@Description("Home Page P Tag Text Visible Verification")
@Severity(SeverityLevel.TRIVIAL)
@Test (priority = 10, description = "Home Page P Tag Text Visible Verification", enabled=true)
public void require_ui_10_home_p_tag_text_verify() throws Exception, Throwable {
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("subline_paragraph_text"))));
				String ptagtext = driver.findElement(By.xpath(obj.getProperty("subline_paragraph_text"))).getText();
				if(ptagtext.contentEquals("This site is dedicated to perform some exercises and demonstrate automated web testing."))
				{
					TestReason = "Pass: Home page P Tag text visible properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Home page P Tag text not visible properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Home page P Tag text not visible properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Form Page Input Box & Submit Button Verification")
@Description("Form Page Input Box & Submit Button Verification")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 11, description = "Form Page Input Box & Submit Button Verification", enabled=true)
public void require_ui_11_form_input_box_submit_button_verify() throws Exception, Throwable {
			form_menu_link();
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("form_contain"))));
				if(driver.findElements(By.xpath(obj.getProperty("form_name_text_field"))).size() != 0 &
				   driver.findElements(By.xpath(obj.getProperty("form_submit_button"))).size() != 0)
				{
					TestReason = "Pass: Form page input box & submit button visible properly";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Form page input box & submit button not visible properly";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
			}
			catch(Exception e)
			{
				TestReason = "Fail: Form page input box & submit button not visible properly";
				logger.info(TestReason);
				myscreenshot(driver);
				Assert.fail(TestReason);
			}	
}
@Story("Form Page Input & Output Verification")
@Description("Form Page Input & Output Verification")
@Severity(SeverityLevel.NORMAL)
@Test (priority = 12, description = "Form Page Input & Output Verification", enabled=true)
public void require_ui_12_form_page_input_output_verify() throws Exception, Throwable {
			String[] inputvalue = {"John", "Sophia","Charlie","Emily"};
			int size = inputvalue.length;

			for(int i=0; i<size; i++)
			{
				inputname = inputvalue[i];
				name_text_field();
				go_submit_button();
				outputtext = driver.findElement(By.xpath(obj.getProperty("form_output_text"))).getText();
				if(outputtext.contentEquals("Hello "+ inputname +"!"))
				{
					TestReason = "Pass: Output display proper for input <"+inputname+">";
					logger.info(TestReason); 
					myscreenshot(driver);
				}
				else
				{
					TestReason = "Fail: Output not display proper for input <"+inputname+">";
					logger.info(TestReason);
					myscreenshot(driver);
					Assert.fail(TestReason);
				}
				driver.navigate().back();
			}	
}
}