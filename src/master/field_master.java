package master;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class field_master extends configue{
	
public void dockler_logo() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("logo_link"))));
			driver.findElement(By.xpath(obj.getProperty("logo_link"))).click();
			logger.info("Logo Click Successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Logo not found.";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}	
public void home_menu_link() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("home_link"))));
			driver.findElement(By.xpath(obj.getProperty("home_link"))).click();
			logger.info("Home menu link click successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Home menu link not found.";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
public void form_menu_link() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("form_link"))));
			driver.findElement(By.xpath(obj.getProperty("form_link"))).click();
			logger.info("Form menu link click successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Form menu link not found.";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
public void error_menu_link() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("error_link"))));
			driver.findElement(By.xpath(obj.getProperty("error_link"))).click();
			logger.info("Error menu link click successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Error menu link not found.";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
public void name_text_field() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("form_name_text_field"))));
			driver.findElement(By.xpath(obj.getProperty("form_name_text_field"))).clear();
			driver.findElement(By.xpath(obj.getProperty("form_name_text_field"))).sendKeys(inputname);
			logger.info("Name inserted successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Name field not found.";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
public void go_submit_button() throws Exception 
{
		try
		{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(obj.getProperty("form_submit_button"))));
			driver.findElement(By.xpath(obj.getProperty("form_submit_button"))).click();
			logger.info("Go button click successfully.");
		}
		catch(Exception e)
		{
			TestReason = "Go button not found.";
			logger.info(TestReason);
			myscreenshot(driver);
			Assert.fail(TestReason);
		}
}
}
